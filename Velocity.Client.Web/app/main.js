﻿"use strict";

var vcApp = angular.module('vcApp', ['ui.router', 'ngAnimate', 'ngMaterial']);


vcApp.config(function ($stateProvider, $mdThemingProvider, $urlRouterProvider) {

    $mdThemingProvider.theme('default')
    .primaryPalette('blue',
    {
        'default': '700',
        'hue-1': '800',
        'hue-2': '900',
        'hue-3': '400'

    })
    .accentPalette('blue-grey');


    $urlRouterProvider.otherwise("bill");
    $stateProvider
        .state('bill', {
            url: '/bill',
            templateUrl: "app/features/createBill/vcCreateBill.html",
            controller: function($scope, addBillItem, billManager, bill, staffDataService) {
                $scope.newBillItem = addBillItem;

                staffDataService.get().success(function (data) {
                    console.log(data);
                    $scope.staff = data;
                });
                

                $scope.items = bill.items;
                $scope.bill = bill;

                $scope.addItem = function() {
                    billManager.addItem();
                }

                $scope.saveBill = function() {
                    billManager.sendBill();
                }
            }
        });

});