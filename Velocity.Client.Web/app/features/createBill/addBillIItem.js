﻿vcApp.factory('addBillItem', function() {
    var service = {
        reset: reset,
        value: 0,
        quantity: 0,
        name: ""
    };

    function reset() {
        service.value = 0;
        service.quantity = 0;
        service.name = "";
    }

    return service;

});

vcApp.factory('billManager', function(bill, addBillItem, billDataService){
   var service = {
       addItem: addItem,
       sendBill: sendBill
   }

    function sendBill() {
        billDataService.sendBill(bill.build()).success(function(data) {
            bill.reset();
        });


    }

   function addItem() {

       var billItem = {
           Value: addBillItem.value,
           Quantity: addBillItem.quantity,
           Name: addBillItem.name,
           DateTime: new Date()
        }

       bill.items.push(billItem);

       addBillItem.reset();
   }
    return service;
});


vcApp.factory('bill', function() {

    var model = {
        items: [],
        customerEmail: "",
        selectedStaffId: {},

        build: build,
        reset: reset
    }

    function reset() {
        model.item = [];
        model.customerEmail = "";
        model.selectedStaffId = 0;
    }

    function build() {
        console.log("Building Bill from model ", model);
        return {
            customerEmail: model.customerEmail,
            lineItems: model.items,
            staff: {
                id: model.selectedStaffId
            }
        }
    }
    return model;
});

vcApp.factory('billDataService', function ($http) {

    var service = {
        sendBill: sendBill    
    }

    function sendBill(bill) {
        console.log("Bill Data", bill);

        return $http.post('../Velocity.WebApi/api/bill/post', bill);
    }

    return service;
});

vcApp.factory('staffDataService', function($http) {
    var service = {
        get: get
    }

    function get() {
        return $http.get('../Velocity.WebApi/api/staff/get');
    }

    return service;
})