﻿CREATE TABLE [dbo].[Bill] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [DateTime]      DATETIME       NOT NULL,
    [StaffId]       INT            NULL,
    [CustomerEmail] NVARCHAR (200) NULL,
    [EmailSent]     BIT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([StaffId]) REFERENCES [dbo].[Staff] ([Id])
);

