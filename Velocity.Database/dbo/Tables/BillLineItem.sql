﻿CREATE TABLE [dbo].[BillLineItem] (
    [Id]       INT             IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (50)   NOT NULL,
    [Value]    DECIMAL (10, 4) NOT NULL,
    [Quantity] INT             NOT NULL,
    [BillId]   INT             NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([BillId]) REFERENCES [dbo].[Bill] ([Id])
);

