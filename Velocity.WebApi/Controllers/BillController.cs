﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Velocity.Core;
using Velocity.Data;

namespace Velocity.WebApi.Controllers
{
    [EnableCors("*", "*", "GET, POST, DELETE")]
    public class BillController : ApiController
    {
        private readonly IBillDataService _billDataService;

        public BillController(IBillDataService billDataService)
        {
            _billDataService = billDataService;
        }

        public IHttpActionResult Post(Bill bill)
        {
            _billDataService.Insert(bill);
            return Ok();
        }

    }
}
