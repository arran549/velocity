﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Velocity.Core;
using Velocity.Data;

namespace Velocity.WebApi.Controllers
{
    public class StaffController : ApiController
    {
        private readonly IStaffDataService _staffDataService;

        public StaffController(IStaffDataService staffDataService)
        {
            _staffDataService = staffDataService;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            var staff =_staffDataService.Get().ToArray();

            //var staff = new Staff[] {new Staff() {FirstName = "arran", LastName = "dyer"}};
            return Ok(staff);
        }
    }
}
