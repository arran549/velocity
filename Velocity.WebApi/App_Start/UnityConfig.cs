using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;
using Velocity.Data;

namespace Velocity.WebApi
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IStaffDataService, StaffDataService>();
            container.RegisterType<IBillDataService, BillDataService>();
            container.RegisterType<BillDataMapper>();
            container.RegisterType<StaffDataMapper>();


            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}