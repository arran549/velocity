﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Velocity.Core;
using Assert = NUnit.Framework.Assert;


namespace Velocity.Data.Tests
{
    [TestFixture]
    public class BillDataServiceTests
    {
        private BillDataService _target;

        [SetUp]
        public void Init()
        {
            _target = new BillDataService(new BillDataMapper(new StaffDataMapper()));
        }


        [Test]
        public void SaveBill()
        {
            //arrange
            var bill = new Bill()
            {
                Staff = new Staff() {Id = 1},
                DateTime = DateTime.Now,
                EmailSent = false,
                CustomerEmail = "arran549@gmail.com",
                LineItems = new List<BillLineItem>()
                {
                    new BillLineItem()
                    {
                        Value = 100,
                        Quantity = 2,
                        Name = "Roast Dinner"
                    }
                }
            };

            //act
            var id = _target.Insert(bill);

            //assert
            Assert.GreaterOrEqual(1, id);
        }

        [Test]
        public void UpdateSentEmail()
        {
            var billId = 1;

            _target.UpdateSentEmail(billId, true);

            var isSent = _target.LoadBill(billId).EmailSent;

            Assert.True(isSent);
        }
    }


    [TestFixture]
    public class StaffDataServiceTests
    {

        [Test]
        public void SaveStaff()
        {
            var staff =  new Staff()
            {
                FirstName = "Arran",
                LastName = "Dyer"
            };

            var target = new StaffDataService(new StaffDataMapper());

            //act
            var id = target.Insert(staff);


            Assert.GreaterOrEqual(1, id);
        }


    }
}
