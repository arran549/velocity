using System.Collections.Generic;
using Velocity.Core;

namespace Velocity.Data
{
    public interface IBillDataService
    {
        int Insert(Bill bill);
        List<Bill> LoadBills();
        Bill LoadBill(int billId);
        List<Bill> LoadUnsentBills();
        void UpdateSentEmail(int billId, bool isSent);
    }
}