using System.Collections.Generic;
using Velocity.Core;

namespace Velocity.Data
{
    public interface IStaffDataService
    {
        int Insert(Staff staff);
        List<Staff> Get();
    }
}