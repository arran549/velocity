﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Velocity.Core
{
    //Aggregate Root
    public class Bill
    {
        public int Id { get; set; }
        public List<BillLineItem> LineItems  { get; set; }
        public DateTime DateTime { get; set; }
        public string CustomerEmail { get; set; }
        public bool EmailSent { get; set; }
        public Staff Staff { get; set; }

//primitive 

    }

    public class BillLineItem
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
        public int Quantity { get; set; }
    }


    public class Staff
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}


