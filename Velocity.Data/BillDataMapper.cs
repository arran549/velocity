using System.Linq;
using Velocity.Core;
using Velocity.Data.EF;

namespace Velocity.Data
{
    public class BillDataMapper : IDataMapper<Bill, dbBill>
    {
        private readonly StaffDataMapper _staffDataMapper;

        public BillDataMapper(StaffDataMapper staffDataMapper)
        {
            _staffDataMapper = staffDataMapper;
        }

        public dbBill MapToDb(Bill domain)
        {
            return new dbBill()
            {
                DateTime = domain.DateTime,
                BillLineItems = domain.LineItems.Select(x => new dbBillLineItem()
                {
                    Name = x.Name,
                    Quantity = x.Quantity,
                    Value = x.Value
                }).ToList(),
                CustomerEmail = domain.CustomerEmail,
                EmailSent = domain.EmailSent,
                StaffId = domain.Staff.Id
            };
        }

        public Bill MapToDomain(dbBill db)
        {
            return new Bill()
            {
                Staff = _staffDataMapper.MapToDomain(db.Staff),
                DateTime = db.DateTime,
                EmailSent = db.EmailSent,
                LineItems = db.BillLineItems.Select(x => new BillLineItem()
                {
                    Quantity = x.Quantity,
                    Value = x.Value,
                    Name = x.Name
                }).ToList(),
                CustomerEmail = db.CustomerEmail
            };
        }
    }
}