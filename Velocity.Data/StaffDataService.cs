using System.Collections.Generic;
using System.Linq;
using Velocity.Core;
using Velocity.Data.EF;

namespace Velocity.Data
{
    public class StaffDataService : IStaffDataService
    {
        private readonly StaffDataMapper _staffDataMapper;
        private VelocityEntities _db = new VelocityEntities();

        public StaffDataService(StaffDataMapper staffDataMapper)
        {
            _staffDataMapper = staffDataMapper;
        }

        public int Insert(Staff staff)
        {
            var dbStaff = _staffDataMapper.MapToDb(staff);

            _db.dbStaffs.Add(dbStaff);

            _db.SaveChanges();

            return dbStaff.Id;
        }

        public List<Staff> Get()
        {
            //return new List<Staff>();

            var dbStaff = _db.dbStaffs.AsEnumerable().ToList();

            List<Staff> staff = new List<Staff>();

            foreach (var s in dbStaff)
            {
                staff.Add(_staffDataMapper.MapToDomain(s));
            }

            //return _db.dbStaffs.ToList().Select(x => _staffDataMapper.MapToDomain(x)).ToList();
            return staff;
        }

    }
}