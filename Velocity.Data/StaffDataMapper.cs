﻿using Velocity.Core;
using Velocity.Data.EF;

namespace Velocity.Data
{
    public class StaffDataMapper : IDataMapper<Staff, dbStaff>
    {
        public dbStaff MapToDb(Staff domain)
        {
            return new dbStaff()
            {
                FirstName = domain.FirstName,
                LastName = domain.LastName
            };
        }

        public Staff MapToDomain(dbStaff db)
        {
            return new Staff()
            {
                Id = db.Id,
                FirstName = db.FirstName,
                LastName = db.LastName
            };
        }
    }
}