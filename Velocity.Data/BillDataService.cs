using System.Collections.Generic;
using System.Linq;
using Velocity.Core;
using Velocity.Data.EF;

namespace Velocity.Data
{
    public class BillDataService : IBillDataService
    {
        private readonly BillDataMapper _billDataMapper;
        private VelocityEntities _db = new VelocityEntities();

        public BillDataService(BillDataMapper billDataMapper)
        {
            _billDataMapper = billDataMapper;
        }

        public int Insert(Bill bill)
        {
            var dbBill = _billDataMapper.MapToDb(bill);

            _db.dbBills.Add(dbBill);
            _db.SaveChanges();
            return bill.Id;
        }

        public List<Bill> LoadBills()
        {
            var bills = _db.dbBills.Select(x => _billDataMapper.MapToDomain(x));
            return bills.ToList();
        }

        public Bill LoadBill(int billId)
        {
            var dbBill = _db.dbBills.Single(x => x.Id == billId);

            return _billDataMapper.MapToDomain(dbBill);
        }

        public List<Bill> LoadUnsentBills()
        {
            var bills = _db.dbBills.Where(x => !x.EmailSent).Select(x => _billDataMapper.MapToDomain(x));
            return bills.ToList();
        }

        public void UpdateSentEmail(int billId, bool isSent)
        {
            _db.dbBills.Single(x => x.Id == billId).EmailSent = isSent;
            _db.SaveChanges();
        }
    }
}