﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace Velocity.Data
{
    public interface IDataMapper<TDomain, TDb>
    {
        TDb MapToDb(TDomain domain);
        TDomain MapToDomain(TDb db);
    }
}
